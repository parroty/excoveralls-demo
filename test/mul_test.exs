defmodule MulTest do
  use ExUnit.Case

  test "multiplication" do
    assert ExcoverallsDemo.mul(1, 2) == 2
  end
end
